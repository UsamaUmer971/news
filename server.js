import axios from "axios";
import  cheerio  from "cheerio";
import  express  from "express";
import bodyParser from "body-parser";

const PORT = process.env.PORT || 5000
const app=express();
// app.use(bodyParser.json());
const articals = [];
axios('https://www.geo.tv/')
    .then(res => {
        const htmlData = res.data;
        const $ = cheerio.load(htmlData);
       
        $('.heading',htmlData).each((index, element)=>{
            const title = $(element).children('.open-section').text()
            const titleURL = $(element).children('.open-section').attr('href')
            articals.push({
                title,
                titleURL
            })
        }) ;
        console.log(articals)
    }).catch(err => console.err(err));

app.get('/data', (req,res)=>{
    res.json(articals);
})
app.listen(PORT, ()=>{
    console.log(`Server running port ${PORT}`)
})